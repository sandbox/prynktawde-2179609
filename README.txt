Description
-----------

This module Creates a permission scheme by roles for editing node titles.

Requirements
------------
Drupal 7.x

Installation
------------
1. Copy the entire node title restrict directory the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"


Configurations
--------------

1. After Installation you need to click on configure link and select the content type which you want to give permission for restrict node title permission for different roles.

2. go to people > permissions > select Term title Permission User Access for user for unchecked to roles which you want retrict node title access.






